# King of the Hill

![](/images/overview.png)

### What is it about
Capturing strategic locations throughout various regions and planets provides factions with a strong presence in the area. It is your job to secure and control the hill for sufficient time in order to capture it.

### In-game user settings
1. Use a map specific hill location and size
2. Hill Radius
3. Starting delay
4. Where the hill's center will be
5. Score needed to win
6. Tech unlock delay
7. Tech unlock speed

### Useful facts about the mod
The hill spawns after a short delay (which can be changed in the lobby menu), with a warning provided at half of the delay to the players. 
The location of the hill is either put in the center of the map, or by taking the average location of all the spawning locations available in the map. 

The hill is visualised with a white circle. The radius of the circle depends on the size of the map. Some maps have a predefined radius. The hill can have up to two inner circles. The first inner circle is white when the hill is not conquered by a player, it is green when the hill is controlled and it is red when the hill is contested. The second inner circle only shows up when there is a Commander on the hill.

One point is given to for every 30 consecutive seconds of control of the hill.

By default only tech 1 is unlocked. New tech levels are unlocked after the hill has been held for a set amount of time (which can be changed with the tech unlock speed setting). After the score threshold has been reached the next tech level will be unlocked to the enemies of the king and after the tech unlock delay (generally 4 minutes) the king and their allies get access to the new tech.

### Installation guide
The standalone game Supreme Commander: Forged Alliance is required. This modification does not support the base game Supreme Commander. You do not need the base game.

Clone or download this repository. After downloading, unzip the file. Open a new window and navigate towards your _My Documents_ folder. From there, navigate to: 
``` sh
".../My Documents/My Games/Gas Powered Games/Supreme Commander: Forged Alliance/Mods"
```
If the _Mods_ folder does not exist, create it. In the _King of the Hill_ folder move the contents of _Mods_ into your Supreme Commander _Mods_ folder. The mod will now be available ingame.

### Credit
Original mod created by: Willem Bart Wijnia

Edit made by: James Seibel

<pretty image>
