

-- import("/lua/lazyvar.lua").ExtendedErrorMessages = true

-- reason for why this is here in interface.lua
interface = { }

local baseCreateUI = CreateUI;
function CreateUI(isReplay) 
	baseCreateUI(isReplay) 
	
	local parent = import('/lua/ui/game/borders.lua').GetMapGroup()
	ForkThread(
		function()
			import('/mods/supcom-koth/modules/interface.lua').CreateModUI(isReplay, parent)
		end
	);


end